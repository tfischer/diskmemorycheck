/*
 * Copyright (2017) Thomas Fischer <fischer@unix-ag.uni-kl.de>.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#define _GNU_SOURCE

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/sysinfo.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/// Small inline helper function
#define min(a,b) ((a)<(b)?(a):(b))

/// 64KB is minimum memblock size
const size_t min_memblocksize = 1 << 16;
/// 64MB is maximum memblock size,
/// may be set later to a different based on the actual physical memory size
size_t max_memblocksize = 1 << 26;
const u_int32_t magic_cookie = 0xdeadbeef;

#define PRINT_PROGRESS_FLAG_DEFAULT   0
#define PRINT_PROGRESS_FLAG_FLUSH     1

unsigned char *memblockBase = NULL;

long long current_time_ms() {
    long long result = 0;
    struct timespec tp;

    errno = 0;
    const int clock_gettime_r = clock_gettime(CLOCK_MONOTONIC, &tp);
    if (clock_gettime_r != 0 || errno != 0)
        return -1;

    result = tp.tv_sec * 1000L + tp.tv_nsec / 1000000L;

    return result;
}

int print_data_size(const size_t data_size, char *output, const size_t output_size) {
    if ((data_size & 0xFFFFFFFFFF) == 0 && (data_size & 0xFFFFF0000000000) > 0) {
        /// full TiB
        return snprintf(output, output_size, "%zu TiB", data_size >> 40);
    } else if ((data_size & 0x3FFFFFFF) == 0 && (data_size & 0xFFFFC0000000) > 0) {
        /// full GiB
        return snprintf(output, output_size, "%zu GiB", data_size >> 30);
    } else if ((data_size & 0xFFFFF) == 0 && (data_size & 0xFFFFF00000) > 0) {
        /// full MiB
        return snprintf(output, output_size, "%zu MiB", data_size >> 20);
    } else if ((data_size & 0x3FF) == 0 && (data_size & 0xFFFFC00) > 0) {
        /// full KiB
        return snprintf(output, output_size, "%zu KiB", data_size >> 10);
    } else if (data_size < 1500L) {
        /// just a few bytes
        return snprintf(output, output_size, "%zu B", data_size);
    } else if (data_size < 1500L * 1024) {
        /// a fractional amount of KiB
        return snprintf(output, output_size, "%.1lf KiB", data_size / 1024.0);
    } else if (data_size < 1500L * 1024 * 1024) {
        /// a fractional amount of MiB
        return snprintf(output, output_size, "%.1lf MiB", data_size / 1024.0 / 1024);
    } else if (data_size < 1500L * 1024 * 1024 * 1024) {
        /// a fractional amount of GiB
        return snprintf(output, output_size, "%.1lf GiB", data_size / 1024.0 / 1024 / 1024);
    } else {
        /// a fractional amount of TiB
        return snprintf(output, output_size, "%.1lf TiB", data_size / 1024.0 / 1024 / 1024 / 1024);
    }
}

struct progress_t {
    size_t acc_data_size;
    long long acc_data_time_ms;
    int call_counter;
};

void init_progress(struct progress_t *progress) {
    if (progress != NULL) {
        progress->acc_data_size = 0;
        progress->acc_data_time_ms = 0;
        progress->call_counter = 0;
    }
}

void step_progress(struct progress_t *progress, const size_t data_size, const long long data_time_ms) {
    if (progress != NULL) {
        progress->acc_data_size += data_size;
        progress->acc_data_time_ms += data_time_ms;
    }
}

void print_progress(struct progress_t *progress, const int flags) {
    if (progress == NULL) return;

    static const size_t buffer_size = 256;
    char buffer[buffer_size];

    if (progress->call_counter % 16 < 15 && (flags & PRINT_PROGRESS_FLAG_FLUSH) == 0) {
        ++progress->call_counter;
        printf(".");
        fflush(stdout);
    } else if (progress->acc_data_time_ms > 0) {
        progress->call_counter = 0;
        print_data_size(progress->acc_data_size * 1000 / progress->acc_data_time_ms, buffer, buffer_size);
        printf(" %s/s\n", buffer);
        progress->acc_data_size = 0;
        progress->acc_data_time_ms = 0;
    } else {
        progress->call_counter = 0;
        print_data_size(progress->acc_data_size, buffer, buffer_size);
        printf(" ");
        puts(buffer);
        progress->acc_data_size = 0;
        progress->acc_data_time_ms = 0;
    }
}

void fill_memblock() {
    /// Determine how many bytes you get per call to rand():
    /// The maximum value returned by rand() is RAND_MAX,
    /// so the number of bytes is min(ceil(log_256(RAND_MAX)), sizeof(int))
    static size_t bytes_per_rand_call = 0;
    if (bytes_per_rand_call == 0) {
        int r = RAND_MAX;
        while (r > 0 && bytes_per_rand_call < sizeof(int)) {
            ++bytes_per_rand_call;
            r >>= 8;
        }
    }

    /// Fill memory block with random data with as many bytes
    /// per step as rand() returns, by copying the returned int
    /// into the memory block
    for (int p = (max_memblocksize / bytes_per_rand_call - 1) * bytes_per_rand_call; p >= 0; p -= bytes_per_rand_call) {
        const int r = rand();
        memcpy(memblockBase + p, &r, bytes_per_rand_call);
    }
    /// If the memory block's size modulo the number of bytes
    /// from a rand() call is not zero, copy only a smaller
    /// number of bytes to completely fill the memory block
    const int r = rand();
    const size_t last_p = max_memblocksize / bytes_per_rand_call * bytes_per_rand_call;
    if (last_p < max_memblocksize)
        memcpy(memblockBase + last_p, &r, max_memblocksize - last_p);
}

size_t compute_max_memblock_size() {
    /// Make a guess what would be a reasonable limit per memory block,
    /// taking into account istalled physical memory and available
    /// physical pages; remember, up to three allocations of this type
    /// may exist in parallel
    const long int avail_phys_mem = getpagesize() * get_avphys_pages();
    const long int phys_mem = getpagesize() * get_phys_pages();
    long int limit = min(avail_phys_mem, phys_mem / 3) / 5;

    /// Perform some shift magic to get the largest number that is
    /// the power of 2 but not larger than 'limit'
    size_t result = 1;
    while (limit > 1) {
        limit >>= 1;
        result <<= 1;
    }

    return result;
}

size_t write_data(int device) {
    unsigned char *memblockCurrent = malloc(max_memblocksize);
    if (memblockCurrent == NULL) { ///< memory allocation failed
        static const size_t buffer_size = 256;
        char buffer[buffer_size];
        print_data_size(max_memblocksize, buffer, buffer_size);
        fprintf(stderr, "Cannot allocate %s for memblockCurrent\n", buffer);
        return 0;
    }
    memcpy(memblockCurrent, memblockBase, max_memblocksize);

    struct progress_t progress;
    init_progress(&progress);
    long long start_time = 0, end_time = 0;
    size_t memblock_size = min_memblocksize;
    size_t total_written_data_size = 0;
    unsigned char alt = 24;

    errno = 0;
    const ssize_t written_magic_cookie_size = write(device, &magic_cookie, sizeof(magic_cookie));
    if (errno != 0 || written_magic_cookie_size != sizeof(magic_cookie))
        goto leave_write_data;
    else
        total_written_data_size += written_magic_cookie_size;

    errno = 0;
    while (errno == 0) {
        start_time = current_time_ms();
        errno = 0;
        const ssize_t written_header_size = write(device, &memblock_size, sizeof(memblock_size));
        if (errno != 0 || written_header_size != sizeof(memblock_size))
            goto leave_write_data;
        else
            total_written_data_size += written_header_size;

        ssize_t pos_in_memblock = 0;
        errno = 0;
        while (errno == 0 && pos_in_memblock < memblock_size) {
            errno = 0;
            const ssize_t written_data_size = write(device, memblockCurrent + pos_in_memblock, memblock_size - pos_in_memblock);
            if (written_data_size > 0 && errno == 0) {
                total_written_data_size += written_data_size;
                pos_in_memblock += written_data_size;
            } else
                goto leave_write_data;
        }
        end_time = current_time_ms();
        const long long diff_time = end_time - start_time;
        step_progress(&progress, memblock_size, diff_time);
        print_progress(&progress, PRINT_PROGRESS_FLAG_DEFAULT);

        for (size_t p = 0; p < memblock_size; ++p) memblockCurrent[p] ^= alt;
        alt += 3; ///< ... and mod 256 as 'alt' is just one byte large

        if (diff_time > 2000 && memblock_size > min_memblocksize)
            memblock_size >>= 1;
        else if (diff_time > 4000 && memblock_size > min_memblocksize * 2)
            memblock_size >>= 2;
        else if (diff_time > 8000 && memblock_size > min_memblocksize * 4)
            memblock_size >>= 3;
        else if (diff_time < 125 && memblock_size < max_memblocksize / 4)
            memblock_size <<= 3;
        else if (diff_time < 250 && memblock_size < max_memblocksize / 2)
            memblock_size <<= 2;
        else if (diff_time < 500 && memblock_size < max_memblocksize)
            memblock_size <<= 1;
    }

leave_write_data:
    end_time = current_time_ms();
    const long long diff_time = end_time - start_time;
    step_progress(&progress, memblock_size, diff_time);
    print_progress(&progress, PRINT_PROGRESS_FLAG_FLUSH);
    free(memblockCurrent);
    return total_written_data_size;
}

size_t read_data(int device, const size_t total_written_data_size) {
    unsigned char *memblockCurrent = malloc(max_memblocksize);
    if (memblockCurrent == NULL) { ///< memory allocation failed
        static const size_t buffer_size = 256;
        char buffer[buffer_size];
        print_data_size(max_memblocksize, buffer, buffer_size);
        fprintf(stderr, "Cannot allocate %s for memblockCurrent\n", buffer);
        return 0;
    }
    unsigned char *memblockDisk = malloc(max_memblocksize);
    if (memblockDisk == NULL) { ///< memory allocation failed
        free(memblockCurrent);
        static const size_t buffer_size = 256;
        char buffer[buffer_size];
        print_data_size(max_memblocksize, buffer, buffer_size);
        fprintf(stderr, "Cannot allocate %s for memblockDisk\n", buffer);
        return 0;
    }
    memcpy(memblockCurrent, memblockBase, max_memblocksize);

    struct progress_t progress;
    init_progress(&progress);
    long long start_time = 0, end_time = 0;
    size_t memblock_size = 0;
    size_t total_read_data_size = 0;
    size_t remaining_data_size = total_written_data_size;
    unsigned char alt = 24;

    u_int32_t my_magic_cookie = 0;
    errno = 0;
    const ssize_t read_magic_cookie_size = read(device, &my_magic_cookie, sizeof(my_magic_cookie));
    if (errno != 0 || read_magic_cookie_size != sizeof(my_magic_cookie) || my_magic_cookie != magic_cookie)
        goto leave_read_data;
    else
        total_read_data_size += read_magic_cookie_size;

    errno = 0;
    while (errno == 0) {
        start_time = current_time_ms();
        errno = 0;
        const ssize_t read_header_size = read(device, &memblock_size, sizeof(memblock_size));
        if (errno != 0 || read_header_size != sizeof(memblock_size) || memblock_size < min_memblocksize || memblock_size > max_memblocksize) {
            memblock_size = 0;
            goto leave_read_data;
        } else
            total_read_data_size += read_header_size;

        ssize_t pos_in_memblock = 0;
        errno = 0;
        while (errno == 0 && pos_in_memblock < memblock_size) {
            errno = 0;
            const ssize_t read_data_size = read(device, memblockDisk + pos_in_memblock, min(memblock_size - pos_in_memblock, remaining_data_size));
            if (read_data_size > 0 && errno == 0) {
                size_t correctly_read_data_size;
                for (correctly_read_data_size = 0; correctly_read_data_size < read_data_size && memblockCurrent[pos_in_memblock + correctly_read_data_size] == memblockDisk[pos_in_memblock + correctly_read_data_size]; ++correctly_read_data_size, ++total_read_data_size);
                if (correctly_read_data_size < read_data_size)
                    goto leave_read_data;
                remaining_data_size -= read_data_size;
                pos_in_memblock += read_data_size;
            } else
                goto leave_read_data;
        }
        end_time = current_time_ms();
        const long long diff_time = end_time - start_time;
        step_progress(&progress, memblock_size, diff_time);
        print_progress(&progress, PRINT_PROGRESS_FLAG_DEFAULT);

        for (size_t p = 0; p < memblock_size; ++p) memblockCurrent[p] ^= alt;
        alt += 3; ///< ... and mod 256 as 'alt' is just one byte large
    }

leave_read_data:
    end_time = current_time_ms();
    const long long diff_time = end_time - start_time;
    step_progress(&progress, memblock_size, diff_time);
    print_progress(&progress, PRINT_PROGRESS_FLAG_FLUSH);
    free(memblockCurrent);
    free(memblockDisk);
    return total_read_data_size;
}

int main(int argc, char *argv[]) {
    static const size_t buffer_size = 256;
    char buffer[buffer_size];

    max_memblocksize = compute_max_memblock_size();

    if (argc == 2 && (strncmp(argv[argc - 1], "-h", 2) == 0 || strncmp(argv[argc - 1], "--help", 6) == 0)) {
        print_data_size(max_memblocksize, buffer, buffer_size);
        fprintf(stderr, "Usage:   diskmemorycheck DEVICE-NODE\nExample:  diskmemorycheck /dev/sdf2\nMaximum size per I/O op:  %s\n", buffer);
        return 1;
    } else if (argc != 2) {
        fprintf(stderr, "Require one argument: a device node in /dev\n");
        return 1;
    } else if (strncmp(argv[argc - 1], "/dev/", 5) != 0) {
        fprintf(stderr, "Argument must start with '/dev/'\n");
        return 1;
    }

    srand(time(NULL));
    memblockBase = malloc(max_memblocksize);
    if (memblockBase == NULL) {
        fprintf(stderr, "Cannot allocate %zu Bytes for memblockBase\n", max_memblocksize);
        return 1;
    }
    fill_memblock();

    errno = 0;
    int device = open(argv[argc - 1], O_WRONLY | O_DSYNC | O_LARGEFILE | O_NOATIME);
    if (device == -1 || errno != 0) {
        fprintf(stderr, "Cannot open '%s' for writing (errno=%d: %s)\n", argv[argc - 1], errno, strerror(errno));
        free(memblockBase);
        return 1;
    }
    const size_t total_written_data_size = write_data(device);
    close(device);
    if (total_written_data_size == 0) {
        fprintf(stderr, "No data written to '%s'\n", argv[argc - 1]);
        free(memblockBase);
        return 1;
    }
    print_data_size(total_written_data_size, buffer, buffer_size);
    printf("Data written: %zu Bytes (%s)\n", total_written_data_size, buffer);

    errno = 0;
    device = open(argv[argc - 1], O_RDONLY | O_LARGEFILE | O_NOATIME);
    if (device == -1 || errno != 0) {
        fprintf(stderr, "Cannot open '%s' for reading (errno=%d: %s)\n", argv[argc - 1], errno, strerror(errno));
        free(memblockBase);
        return 1;
    }
    const size_t total_read_data_size = read_data(device, total_written_data_size);
    close(device);
    if (total_read_data_size == 0) {
        free(memblockBase);
        fprintf(stderr, "No data read from '%s'\n", argv[argc - 1]);
        return 1;
    }
    print_data_size(total_read_data_size, buffer, buffer_size);
    printf("Data correctly read: %zu Bytes (%s)\n", total_read_data_size, buffer);

    if (total_read_data_size != total_written_data_size) {
        free(memblockBase);
        fprintf(stderr, "Amount of data written to '%s' differs from amount of data correctly read from this device: %zu != %zu\n", argv[argc - 1], total_written_data_size, total_read_data_size);
        return 1;
    }

    free(memblockBase); ///< memblockBase is no longer used after this point

    return 0;
}
