TEMPDIR:=/tmp/.diskmemorycheck-compile-$(shell echo "$${PWD}$${UID}" | md5sum | cut -f 1 -d \ )
CFLAGS?=-Wall -pedantic -std=c11
LDFLAGS:=
CSOURCE:=diskmemorycheck.c
HEADERS:=

diskmemorycheck: $(patsubst %.c,$(TEMPDIR)/%.o,$(CSOURCE))
	$(CC) $(LDFLAGS) -o $@ $^

$(TEMPDIR)/%.o: %.c $(HEADERS)
	mkdir -p $(TEMPDIR)
	$(CC) $(CFLAGS) -o $@ -c $<

mrproper: clean
	rm -f diskmemorycheck

astyle:
	astyle -n --align-reference=name --align-pointer=name --indent=spaces=4 --indent-labels --pad-oper --unpad-paren --pad-header --keep-one-line-statements --convert-tabs --indent-preprocessor $(wildcard *.c) $(wildcard *.h)

clean:
	rm -rf $(TEMPDIR)
	rm -f *~
