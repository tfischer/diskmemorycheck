# DiskMemoryCheck

A tool to check if a fresh/new/unused storage device such as a SD card or a USB flash drive can store and recall the advertised storage capacity.
The check is performed by writing as much as possible (pseudo-)random data onto the storage device and then reading the data back while checking that the read data equals the written data.

### Warning

Using this tool on a storage device will destroy all data on it. No recovery will be possible.

## Compile and Install

This tool has been tested on Linux systems. It may work on other Unix-like systems, too.

To compile the code, invoke `make` in the source directory.

To adjust the CFLAGS variable, no modification in the Makefile is necessary.
Simply pass a different CFLAGS variable in the environment to the `make` command.

### Installation

It is not necessary to 'install' this tool; the compiled binary in the source directory will work as it is.
For a more permanent installation, manually copy this binary to `/usr/local/bin` (distribution packages may want to copy it to just `/usr/bin`).

There is no user-targeting documentation like a man page available (yet) except for this document.

## Usage

This tool takes a single argument: a device node inside the `/dev` directory, such as `/dev/sdd` or `/dev/sdf1`.
As performing raw I/O to device nodes usually requires administrator permissions, you will most likely have to invoke this tool with `sudo` or `su`.

During the tools operation of writing to or reading from a storage device, it will print a single dot about once per second to indicate progress.
In larger time steps, the current I/O performance in Bytes per second will be shown.
After completing the write and read operations, respectively, the total amount of written/read data will be shown.